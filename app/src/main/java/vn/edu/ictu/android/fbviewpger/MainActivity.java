package vn.edu.ictu.android.fbviewpger;

import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        ViewPager pager = findViewById(R.id.pager);
        PagerAdapter adapter = new PagerAdapter(getSupportFragmentManager());
        adapter.addFragment(new NewFeedFragment(), "");
        adapter.addFragment(new FriendFragment(), "");
        adapter.addFragment(new NotifyFragment(), "");
        adapter.addFragment(new MenuFragment(), "");

        pager.setAdapter(adapter);
        TabLayout tab = findViewById(R.id.tab);
        tab.setupWithViewPager(pager);
        tab.getTabAt(0).setIcon(R.drawable.ic_newfeed);
        tab.getTabAt(1).setIcon(R.drawable.ic_friend);
        tab.getTabAt(2).setIcon(R.drawable.ic_notiy);
        tab.getTabAt(3).setIcon(R.drawable.ic_menu);
    }
}
